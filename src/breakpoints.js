const bp = {
    s: "576px",
    sm: "768px",
    m: "992px",
    l: "1200px",
    xl: "2000px"
}

export default bp