import React, { useEffect, useState } from "react"
import bp from "../breakpoints"
import styled from 'styled-components'
import loadable from '@loadable/component'
import Countdown from 'react-countdown'

const ReactPlayer = loadable(() => import("react-player"))

const StyledContainer = styled.div`
    font-size: 2em;
    min-height: 400px;
    display: flex;
    align-items: center;
    justify-content: center;
    @media (min-width: ${bp.m}) {
        font-size: 2em;
        align-items: flex-start;
    }
`

const Video = () => {
  return (
      <StyledContainer>
        <Countdown date={new Date("January 1, 2021 15:00:00")} >
            <ReactPlayer
                url='http://stream.bourdellage.live:8000/live/bourdellage_src/index.m3u8'
                playing={false}
                controls={true}
                width="95%"
                height="auto"
            />
        </Countdown>
    </StyledContainer>
 )
}

export default Video
