import React, { useEffect } from "react"
import styled from 'styled-components'
import bp from "../breakpoints"

const ChatContainer = styled.div`
  height: 100%;
  position: relative;
  display:block;
`

const BlockingDiv = styled.div`
  width: 100%;
  height: 25px;
  right: 0%;
  background-color: #64C0FF;
  position: absolute;
`

const Chat = () => {

    useEffect(() => {
        let tag = document.createElement('script');
        tag.appendChild(document.createTextNode('{"handle":"bourdellage","arch":"js","styles":{"a":"64C0FF","b":100,"c":"000000","d":"000000","k":"64C0FF","l":"64C0FF","m":"64C0FF","p":"10","q":"64C0FF","r":100,"surl":0,"cnrs":"0.35"}}'));
        tag.async = false;
        tag.src = 'http://st.chatango.com/js/gz/emb.js';
        tag.id = 'cid0020000269584760503';
        tag.style.width = '100%';
        tag.style.height = '100%';
        let chatContainer = document.getElementsByClassName('chat-container')[0];
        chatContainer.appendChild(tag);
    }, [])

  return (
    <ChatContainer className={`chat-container`} >
      <BlockingDiv/>
    </ChatContainer>
  )
}

export default Chat