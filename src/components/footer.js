import React, { useEffect } from "react"
import styled from 'styled-components'
import bp from "../breakpoints"
import LogoNmore from "../assets/images/logo_nmore.png"
import LogoBourreaux from "../assets/images/logo_bourreaux.png"
import LogoLoveMachine from "../assets/images/logo_lovemachine.png"
import LogoCercle from "../assets/images/logo_cercle.png"

const FooterContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  grid-area: "footer";
  & img {
    max-width: 75px;
  }
  @media (min-width: ${bp.m}) {
    padding-right: 25vw;
    padding-left: 25vw;
    padding-top: 40px;
    & img {
      max-width: 125px;
    }
  }
`

const Footer = () => {
  return (
    <FooterContainer>
      <a href="https://www.facebook.com/NMOREASBL" target="__blank"><img loading="lazy" src={LogoNmore} alt="Logo nmore"></img></a>
      <a href="https://www.facebook.com/battlevinylonly" target="__blank"><img loading="lazy" src={LogoBourreaux} alt="Logo Bourreaux de Béthune"></img></a>
      <a href="https://www.facebook.com/LoveMachine-1674095299288227/" target="__blank"><img loading="lazy" src={LogoLoveMachine} alt="Logo Love Machine"></img></a>
      <a href="https://www.facebook.com/le-ceRCle-103620334451543" target="__blank"><img loading="lazy" src={LogoCercle} alt="Logo Le cercle"></img></a>
    </FooterContainer>
  )
}

export default Footer