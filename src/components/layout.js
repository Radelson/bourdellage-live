 import React, { useEffect, useState } from "react"
 import styled from 'styled-components'
 import GlobalStyle from "../globalstyle"
 import bp from "../breakpoints"
 import Logo from '../assets/svgs/logo.svg'
import Footer from "./footer"
 
 const Content = styled.div`
 grid-area: "content";
 `
 
 const LogoContainer = styled.div`
  grid-area: "logo";
  background: rgb(236,98,223);
  height: 100%;
  /*background: linear-gradient(0deg, rgba(236,98,223,1) 0%, rgba(252,243,154,1) 78%);*/
  background: linear-gradient(0deg, rgb(100, 192, 255) 0%, rgba(236,98,223,1) 78%);
  display: flex;
  align-items: center;
  padding-top: 1em;
  justify-content: center;
  @media (min-width: ${bp.m}) {
    justify-content: center;
  }
 `

 const StyledLogo = styled(Logo)`
    max-width: 85vw;
    @media (min-width: ${bp.m}) {
      max-width: 725px;
    }
 `
 const LayoutContainer = styled.div`
  height: 100%;
  display: grid;
  grid-template-rows: 20vh 80vh 15vh;
  grid-template-columns: 1fr;
  grid-template-areas:
  "logo"
  "content"
  "footer";
  @media (min-width: ${bp.m}) {
    grid-template-rows: 30vh auto 10vh;
    grid-template-columns: 1fr;
    grid-template-areas:
  "logo"
  "content"
  "footer";
   }
 `
 const Layout = ({ children }) => {
 
   return (
      <LayoutContainer>
         <GlobalStyle/>
         <LogoContainer>
          <StyledLogo/>
        </LogoContainer>
        <Content>
           {children}
        </Content>
        <Footer/>
      </LayoutContainer>
   )
 }
 
 export default Layout
 