import { createGlobalStyle } from "styled-components"
import Breakpoints from "./breakpoints"
import Sixtyfour from "./fonts/sixtyfour.ttf"



export default createGlobalStyle`
  body {
    margin: 0px;
    height: 100%;
    /*background: rgba(236,98,223,1);*/
    background: rgb(100, 192, 255);
  }

  html {
    height: 100%;
    @font-face {
      font-family: 'Sixtyfour';
      src: url(${Sixtyfour}) format('truetype');
    }
    font-family: 'Sixtyfour', sans-serif;
  }

  h2, h3, h4, h5, h6 {
    font-weight: 268;
  }

  h1 {
    font-size: 58px;
  }

  h2 {
    line-height: 1em;
    font-size: 42px;
  }

  a {
    text-decoration: none;
  }

  #___gatsby {
    height: 100%;
  }

  #gatsby-focus-wrapper {
    height: 100%;
  }

  div[role="group"][tabindex] {
    height: 100%;
  }
`
