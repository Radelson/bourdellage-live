import * as React from "react"
import Layout from "../components/layout"
import styled from 'styled-components'
import bp from "../breakpoints"
import Chat from "../components/chat"
import Video from "../components/video-stream"

const ChatContainer = styled.div`
    grid-area: 'chat';
`
const VideoContainer = styled.div`
    grid-area: 'video';
    display: flex;
    align-items: center;
    justify-content: center;
`

const StreamPageContainer = styled.div`
     display: grid;
     height: 100%;
     grid-template-rows: 45vh 35vh;
     grid-template-columns: auto;
     grid-template-areas:
     "video"
     "chat";
     @media (min-width: ${bp.m}) {
       grid-template-rows: auto;
       grid-template-columns: 3fr 1fr;
       grid-template-areas:
       "video chat";
   }
`

const IndexPage = () => {
  return (
    <Layout>
      <StreamPageContainer>
        <VideoContainer>
          <Video/>
        </VideoContainer>
        <ChatContainer>
          <Chat/>
        </ChatContainer>
      </StreamPageContainer>
    </Layout>
  )
}

export default IndexPage
